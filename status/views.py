from django.shortcuts import render, redirect, HttpResponse
from .forms import StatusForm
from .models import Status

# Create your views here.

def index(request):
    status = Status.objects.all();
    form = StatusForm()
    context = {
        'status': status,
        'form': form
    }
    return render(request, "status/index.html", context)

def confirmation(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        context = {
            'form': form,
            'name': request.POST['name'],
            'status': request.POST['status']
        }       
        return render(request, "status/confirmation.html", context)
    else:
        return redirect('status:index')

def add_status(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('status:index')
    else:
        return redirect('status:index')

def change_color(request,pk):
    colorList = ['#fbfbfb', '#fff1c1', '#f4f3f3', '#78b7bb', '#808b97']
    status = Status.objects.get(id=pk)
    colorIndex = (colorList.index(status.color) + 1) % 5 + 1
    newColor = colorList[colorIndex-1]
    status.color = newColor
    status.save()
    return redirect('status:index')

