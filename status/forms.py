from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['name', 'status']
        widgets = {
            'name' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Your Name"}),
            'status' : forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Wanna share something?',
                'rows':'4'
            })
        }


