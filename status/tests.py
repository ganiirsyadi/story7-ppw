from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import Status
from .forms import StatusForm
from .views import index, confirmation, add_status, change_color

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class ModelTest(TestCase):

    def setUp(self):
        self.status = Status.objects.create(name="joko", status="Halo guys, I am Joko")

    def test_instance_created(self):
        self.assertEqual(Status.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.status), "joko")

class FormTest(TestCase):

    def test_form_is_valid(self):
        form = StatusForm(data = {
            "name" : "koala",
            "status" : "halo aku koala dari Australia"
        })
        self.assertTrue(form.is_valid())
    
    def test_form_invalid(self):
        form = StatusForm(data = {})
        self.assertFalse(form.is_valid())

class UrlsTest(TestCase):

    def setUp(self):
        self.status = Status.objects.create(name="joko", status="Halo guys, I am Joko")
        self.index = reverse("status:index")
        self.confirmation = reverse("status:confirmation")
        self.add_status = reverse("status:add_status")
        self.change_color = reverse("status:change_color", args=[self.status.id])
    
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_confirmation_use_right_function(self):
        found = resolve(self.confirmation)
        self.assertEqual(found.func, confirmation)
    
    def test_add_status_use_right_function(self):
        found = resolve(self.add_status)
        self.assertEqual(found.func, add_status)

    def test_change_color_use_right_function(self):
        found = resolve(self.change_color)
        self.assertEqual(found.func, change_color)


class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.status = Status.objects.create(name="joko", status="Halo guys, I am Joko")
        self.index = reverse("status:index")
        self.confirmation = reverse("status:confirmation")
        self.add_status = reverse("status:add_status")
        self.change_color = reverse("status:change_color", args=[self.status.id])

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "status/index.html")

    def test_POST_confirmation(self):
        response = self.client.post(self.confirmation, 
        {
            'name': 'naruto',
            'status': 'PPW adalah jalan ninjaku'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "status/confirmation.html")
        self.assertContains(response, "naruto")
    
    def test_POST_add_status(self):
        response = self.client.post(self.add_status,
        {
            'name': 'naruto',
            'status': 'PPW adalah jalan ninjaku'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "naruto")

    def test_GET_confirmation(self):
        response = self.client.get(self.confirmation, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "status/index.html")

    def test_GET_add_status(self):
        response = self.client.get(self.add_status, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "status/index.html")

    def test_GET_change_status(self):
        response = self.client.get(self.change_color, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "#fff1c1")


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_title(self):
        selenium = self.selenium
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        self.assertEqual(selenium.title, "People Stories")

    def test_add_status(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 60)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # find the form
        add_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-outline-success')))
        add_button.click()
        name = wait.until(EC.element_to_be_clickable((By.ID, 'id_name')))
        status = wait.until(EC.element_to_be_clickable((By.ID, 'id_status')))
        post_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer button")))
        # fill the form
        name.send_keys("Gani")
        status.send_keys("Halo namaku gani aku dari jepara")
        # click post button to submit the form
        post_button.click()
        # confirm the post
        confirm_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.btn.btn-success.mr-2")))
        confirm_button.click()
        # Check new post exist
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-outline-success')))
        self.assertIn("Gani", selenium.page_source)
        self.assertIn("Halo namaku gani aku dari jepara", selenium.page_source)

    def test_cancel_status(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 60)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # find the form
        add_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-outline-success')))
        add_button.click()
        name = wait.until(EC.element_to_be_clickable((By.ID, 'id_name')))
        status = wait.until(EC.element_to_be_clickable((By.ID, 'id_status')))
        post_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer button")))
        # fill the form
        name.send_keys("Allam")
        status.send_keys("Halo namaku allam aku dari jepara")
        # click post button to submit the form
        post_button.click()
        # confirm the post
        confirm_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "a.btn.btn-secondary.mr-2")))
        confirm_button.click()
        # Check new post exist
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-outline-success')))
        self.assertNotIn("Allam", selenium.page_source)
        self.assertNotIn("Halo namaku allam aku dari jepara", selenium.page_source)
    
    def test_change_color(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 60)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/')
        # find the form
        add_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-outline-success')))
        add_button.click()
        name = wait.until(EC.element_to_be_clickable((By.ID, 'id_name')))
        status = wait.until(EC.element_to_be_clickable((By.ID, 'id_status')))
        post_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer button")))
        # fill the form
        name.send_keys("Gani")
        status.send_keys("Halo namaku gani aku dari jepara")
        # click post button to submit the form
        post_button.click()
        # confirm the post
        confirm_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.btn.btn-success.mr-2")))
        confirm_button.click()
        # Check new post exist
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-outline-success')))
        self.assertIn("Gani", selenium.page_source)
        self.assertIn("Halo namaku gani aku dari jepara", selenium.page_source)
        # change post color
        confirm_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "body > div.d-flex.flex-column.justify-content-start.pt-5.align-items-center.container.wrapper.pb-5 > div:nth-child(1) > a")))
        confirm_button.click()
        self.assertIn("#fff1c1", selenium.page_source)
        
        








    
       
