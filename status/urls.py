from django.urls import path

from . import views

app_name = 'status'

urlpatterns = [
    path('', views.index, name='index'),
    path('confirmation/', views.confirmation, name='confirmation'),
    path('confirmation/add', views.add_status, name='add_status'),
    path('color/<str:pk>', views.change_color, name='change_color')
]